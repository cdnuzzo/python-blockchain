# initialze blockchain list
blockchain = []

def get_last_blockchain_value():
    """ Returns the last value of the current blockchain. """
    if len(blockchain) < 1:
        return None
    return blockchain[-1]

def add_transaction(transaction_amount, last_transaction=[1]):
    """ Append a new value as well as the last blockchain value
    Arguments:
        :transaction_amount: The amount that should be added.
        :last_transaction: The last blockchain transaction (default [1])
    """
    if last_transaction == None:
        last_transaction = [1]
    blockchain.append([last_transaction, transaction_amount])

def get_transaction_value():
    return float(input('Your transaction amount please: '))


def get_user_choice():
    user_input = input('your choice: ')
    return user_input

def print_blockchain_element():
    # output the blockchain list to the console
    for block in blockchain:
        print('outputting block')
        print(block)
    else:
        print('-' * 21)


def verify_chain(): 
    # block_index = 0
    is_valid = True
    for block_index in range(len(blockchain)):
        if block_index == 0:
            continue
        elif blockchain[block_index][0] == blockchain[block_index -1]:
            is_valid = True
        else:
            is_valid= False
            break
    # for block in blockchain:
    #     if block_index == 0:
    #         block_index += 1
    #         continue
    #     elif block[0] == blockchain[block_index -1]:
    #         is_valid = True
    #     else:
    #         is_valid= False
    #         break
    #     block_index += 1
    return is_valid

waiting_for_input = True


while waiting_for_input:
    print('please choose')
    print('1: add a new transaction value')
    print('2: output the blockchain blocks')
    print('h: manipulate the chain')
    print('q: Quit')
    user_choice = get_user_choice()
    if user_choice == '1':
        tx_amount = get_transaction_value()
        add_transaction(tx_amount, get_last_blockchain_value())
    elif user_choice == '2':
        print_blockchain_element()
    elif user_choice == 'h':
        if len(blockchain) >=1:
            blockchain[0] = [2]
    elif user_choice == 'q':
        waiting_for_input = False
    else:
        print('input invalid, pick a value from the list')
    if not verify_chain():
        print_blockchain_element()
        print('INVALID BLOCKCHAIN')
        break
else:
    print('user left')

print('done.')