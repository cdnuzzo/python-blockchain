class Car:
    # top_speed = 100
    def __init__(self, starting_top_speed=100): 
        self.top_speed = starting_top_speed
        self.__warnings = []

    def add_warning(self, warning_text):
        if len(warning_text) > 0:
            self.__warnings.append(warning_text)

    def get_warnings(self):
        return self.__warnings

    def drive(self):
        print('i am driving but not faster than {}'.format(self.top_speed))

car1 = Car()
car1.drive()

car1.add_warning('New warning')
# print(car1.__dict__)
print(car1)

car2 = Car(200)
car2.drive()
print(car2.get_warnings())