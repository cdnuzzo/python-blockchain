import functools
# initialze blockchain list
MINING_REWARD = 10


genesis_block = {
    'previous_hash': '',
    'index': 0, 
    'transactions': []
}
blockchain = [genesis_block]
open_transactions = []
owner = 'Craig'
participants = {'Craig'}

def hash_block(block):
    return '-'.join([str(block[key]) for key in block])


def get_balance(participant):
    tx_sender = [[tx['amount'] for tx in block['transactions'] if tx['sender'] == participant] for block in blockchain]
    open_tx_sender = [tx['amount'] for tx in open_transactions if tx['sender'] == participant]
    tx_sender.append(open_tx_sender)
    amount_sent = functools.reduce(lambda tx_sum, tx_amt: tx_sum + sum(tx_amt) if len(tx_amt) > 0 else tx_sum + 0, tx_sender, 0)
    tx_recipient = [[tx['amount'] for tx in block['transactions'] if tx['recipient'] == participant] for block in blockchain]
    amount_received = functools.reduce(lambda tx_sum, tx_amt: tx_sum + sum(tx_amt) if len(tx_amt) > 0 else tx_sum + 0, tx_recipient, 0)
    # return total balance
    return amount_received - amount_sent


def get_last_blockchain_value():
    """ Returns the last value of the current blockchain. """
    if len(blockchain) < 1:
        return None
    return blockchain[-1]


def verify_transaction(transaction):
    sender_balance = get_balance(transaction['sender'])
    return sender_balance >= transaction['amount']

def add_transaction(recipient, sender=owner, amount=1.0):
    """ Append a new value as well as the last blockchain value
    Arguments:
        :sender: The sender of the coins.
        :recipient: The recipient of the coins.
        :amount: The amount of coins sent with the transactions (default = 1.0)
    """
    transaction = {
        'sender': sender, 
        'recipient': recipient, 
        'amount': amount
    }
    if verify_transaction(transaction):
        open_transactions.append(transaction)
        participants.add(sender)
        participants.add(recipient)
        return True
    return False


def mine_block():
    last_block = blockchain[-1]
    hashed_block = hash_block(last_block)
    reward_transaction = {
        'sender': 'MINING',
        'recipient': owner,
        'amount': MINING_REWARD,
    }
    copied_transactions = open_transactions[:]
    copied_transactions.append(reward_transaction)
    block = {
        'previous_hash': hashed_block,
        'index': len(blockchain), 
        'transactions': copied_transactions
    }
    blockchain.append(block)
    return True


def get_transaction_value():
    """ Returns the input of the user (a new transaction amount) as a float."""
    tx_recipient = input('enter the recipient of the transaction: ')
    tx_amount = float(input('your transaction amount please: '))
    return tx_recipient, tx_amount


def get_user_choice():
    user_input = input('your choice: ')
    return user_input

def print_blockchain_element():
    # output the blockchain list to the console
    for block in blockchain:
        print('outputting block')
        print(block)
    else:
        print('-' * 21)


def verify_chain(): 
    for (index, block) in enumerate(blockchain):
        if index == 0:
            continue
        if block['previous_hash'] != hash_block(blockchain[index - 1]):
            return False
    return True

waiting_for_input = True


while waiting_for_input:
    print('please choose')
    print('1: add a new transaction value')
    print('2: mine a new block')
    print('3: output the blockchain blocks')
    print('4: output participants')
    print('h: manipulate the chain')
    print('q: Quit')
    user_choice = get_user_choice()
    if user_choice == '1':
        tx_data = get_transaction_value()
        recipient, amount = tx_data
        # add the transaction amount to the blockchain
        if add_transaction(recipient, amount=amount):
            print('added transaction!')
        else:
            print('transaction failed')
        print(open_transactions)
    elif user_choice == '2':
        if mine_block():
            open_transactions = []
    elif user_choice == '3':
        print_blockchain_element()
    elif user_choice == '4':
        print(participants)
    elif user_choice == 'h':
        if len(blockchain) >=1:
            blockchain[0] = {
                'previous_hash': '',
                'index': 0, 
                'transactions': [{'sender': 'Chris', 'recipient': 'Craig', 'amount': 100.00}]
            }
    elif user_choice == 'q':
        waiting_for_input = False
    else:
        print('input invalid, pick a value from the list')
    if not verify_chain():
        print_blockchain_element()
        print('INVALID BLOCKCHAIN')
        break
    print('Balance of {}: {:6.2f}'.format('Craig', get_balance('Craig')))
else:
    print('user left')

print('done.')